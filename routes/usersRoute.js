const express = require("express");
const router = express.Router();
const usersController = require("../controllers/usersController");
const authentication = require("../auth");


// User Registration route
router.post("/register", (request, response) => {
	usersController.userRegistration(request.body).then(controllerResult => response.send(controllerResult));
});


// User Authentication route
router.post("/login", (request, response) => {
	usersController.userLogin(request.body).then(controllerResult => response.send(controllerResult));
});



// Non-admin User checkout (Create Order)
router.post("/checkout", authentication.tokenVerification, (request, response) => {

	const data = {
		userId: authentication.tokenDecryption(request.headers.authorization).id,
		productId: request.body.productId,
		isAdmin : authentication.tokenDecryption(request.headers.authorization).isAdmin,
		productName: request.body.productName,
		quantity: request.body.quantity

	}

	usersController.createUserOrder(data).then(controllerResult => response.send(controllerResult));

});


// Retrieve User Details
router.get("/:userId/details", authentication.tokenVerification, (request, response) => {

	const userId = authentication.tokenDecryption(request.headers.authorization).id;

	usersController.getUserDetails(userId).then(controllerResult => response.send(controllerResult));

});


// Retrieve ALL orders
router.get("/orders", authentication.tokenVerification, (request, response) => {

	const userData = authentication.tokenDecryption(request.headers.authorization);

	usersController.getAllOrders(userData).then(controllerResult => response.send(controllerResult));

});


// Retrieve my orders
router.get("/myOrders", authentication.tokenVerification, (request,response) => {

	const userData = authentication.tokenDecryption(request.headers.authorization);

	usersController.getMyOrders(userData).then(controllerResult => response.send(controllerResult));

});


// Set Non-Admin User as Admin
router.put("/:userId/setAsAdmin", authentication.tokenVerification, (request, response) => {

	const userData = authentication.tokenDecryption(request.headers.authorization);
	
	console.log(userData);

	usersController.setUserAsAdmin(request.params, userData).then(controllerResult => response.send(controllerResult));

});

// Set Remove as Admin
router.put("/:userId/removeAsAdmin", authentication.tokenVerification, (request, response) => {

	const userData = authentication.tokenDecryption(request.headers.authorization);

	usersController.removeAsAdmin(request.params, userData).then(controllerResult => response.send(controllerResult));

});

// Add to Cart



module.exports = router;