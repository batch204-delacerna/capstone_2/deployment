const Users = require("../models/Users");
const Products = require("../models/Products");
 

// Product registration
module.exports.productRegistration = (requestBody, usersData) => {
 
	return Users.findById(usersData.userId).then(result => {

		if (usersData.isAdmin == false) {
			
			//return false //For REACT.js
			return "Only an admin can register new product.";
		} else {

			let newProducts = new Products({
				name: requestBody.name,
				description: requestBody.description,
				price: requestBody.price
			});

			return newProducts.save().then((product, error) => {

				if (error) {
					
					//return false //For REACT.js
					return "Product registration failed.";
				} else {

					//return true //For REACT.js
					return "Product registration successful!";
				}

			});
		}
	});
}


module.exports.activeProductRetrieval = () => {

	return Products.find({isActive: true}).then(result => {
		return result;
	});
}

module.exports.getProduct = (requestParams) => {

	return Products.findById(requestParams.productId).then(result => {
		return result;
	});

}

module.exports.updateProductInfo = (requestParams, requestBody, usersData) => {

	return Products.findById(requestParams.productId).then(result => {

		if (usersData.isAdmin === true) {

			let productInfoUpdated = {
				name: requestBody.name,
				description: requestBody.description,
				price: requestBody.price,
				isActive: requestBody.isActive
			}

			return Products.findByIdAndUpdate(requestParams.productId, productInfoUpdated).then((product, error) => {

				if (error) {
					
					//return false //For REACT.js
					return "Product information not updated.";

				} else {

					//return true //For REACT.js
					return "Product information successfully updated!";

				}

			});

		} else {

			//return false //For REACT.js
			return "User is not an admin! Cannot update product information."
		}

	});
}

module.exports.archiveProduct = (requestParams, usersData) => {

	return Products.findById(requestParams.productId).then(result => {

		if (usersData.isAdmin === true) {

			let productInfoUpdated = {
				isActive: false
			}

			return Products.findByIdAndUpdate(requestParams.productId, productInfoUpdated).then((course, error) => {

				if (error) {

					//return false //For REACT.js
					return "Product archiving not successful.";

				} else {

					//return true //For REACT.js
					return "Product archiving successful!";

				}

			});

		} else {

			//return false //For REACT.js
			return "User is not an admin! Cannot archive product."

		}

	});

}



module.exports.enableProduct = (requestParams, usersData) => {

	return Products.findById(requestParams.productId).then(result => {

		if (usersData.isAdmin === true) {

			let productInfoUpdated = {
				isActive: true
			}

			return Products.findByIdAndUpdate(requestParams.productId, productInfoUpdated).then((course, error) => {

				if (error) {

					//return false //For REACT.js
					return "Product activation not successful.";

				} else {

					//return true //For REACT.js
					return "Product activation successful!";

				}

			});

		} else {

			//return false //For REACT.js
			return "User is not an admin! Cannot activate product."

		}

	});

}